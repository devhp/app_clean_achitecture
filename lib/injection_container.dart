import 'package:app_clean_architecture/data/data_source/local/app_database.dart';
import 'package:app_clean_architecture/data/data_source/remote/news_api_service.dart';
import 'package:app_clean_architecture/data/repository/article_repository_imp.dart';
import 'package:app_clean_architecture/domain/usecases/get_article_usecase.dart';
import 'package:app_clean_architecture/presentation/bloc/article/remote/remote_article_bloc.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'domain/repository/article_repository.dart';

final sl = GetIt.instance;

Future initializeDependencies() async {

  final database = await $FloorAppDatabase.databaseBuilder('app_database').build();
  sl.registerSingleton<AppDatabase>(database);

  // Dio
  sl.registerSingleton<Dio>(Dio());

  // Dependencies
  sl.registerSingleton<NewsApiService>(NewsApiService(sl()));
  sl.registerSingleton<ArticleRepository>(ArticleRepositoryImp(sl()));

  // UseCases
  sl.registerSingleton<GetArticleUseCase>(GetArticleUseCase(sl()));

  // Bloc
  sl.registerLazySingleton<RemoteArticleBloc>(() => RemoteArticleBloc(sl()));
}
