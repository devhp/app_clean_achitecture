import 'package:app_clean_architecture/presentation/bloc/article/remote/remote_article_bloc.dart';
import 'package:app_clean_architecture/presentation/bloc/article/remote/remote_article_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'widgets/widgets.dart';

class DailyNewsPage extends StatefulWidget {
  const DailyNewsPage({super.key});

  @override
  State<DailyNewsPage> createState() => _DailyNewsPageState();
}

class _DailyNewsPageState extends State<DailyNewsPage> {
  _buildAppBar() {
    return AppBar(
      title: const Text('Daily news'),
    );
  }

  Widget _buildBody() {
    return BlocBuilder<RemoteArticleBloc, RemoteArticleState>(builder: (context, state) {
      switch (state.runtimeType) {
        case RemoteArticleLoading:
          return const Center(
            child: CupertinoActivityIndicator(),
          );
        case RemoteArticleError:
          return const Center(
            child: Icon(Icons.refresh),
          );
        case RemoteArticleDone:
          return ListView.builder(
              itemCount: state.articles!.length,
              itemBuilder: (context, index) {
                return ItemArticleWidget(article: state.articles![index],);
              });
        default:
          return const SizedBox.shrink();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }
}
