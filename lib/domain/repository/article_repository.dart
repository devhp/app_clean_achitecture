import 'package:app_clean_architecture/domain/entites/article_entity.dart';

import '../../core/resources/data_state.dart';

abstract class ArticleRepository{

  Future<DataState<List<ArticleEntity>>> getNewsArticles();
}