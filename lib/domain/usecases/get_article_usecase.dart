import 'package:app_clean_architecture/core/resources/data_state.dart';
import 'package:app_clean_architecture/core/usecases/usecase.dart';
import 'package:app_clean_architecture/domain/entites/article_entity.dart';

import '../repository/article_repository.dart';

class GetArticleUseCase implements UseCase<DataState<List<ArticleEntity>>, void>{

  final ArticleRepository _articleRepository;

  GetArticleUseCase(this._articleRepository);

  @override
  Future<DataState<List<ArticleEntity>>> call({void params}) {
    return _articleRepository.getNewsArticles();
  }
}