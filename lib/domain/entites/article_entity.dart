import 'package:equatable/equatable.dart';

class ArticleEntity extends Equatable {
  final String? author;
  final String? title;
  final String? description;
  final String? url;
  final String? urlToImage;
  final String? publishedAt;
  final String? content;

  const ArticleEntity({this.author, this.title, this.description, this.url, this.urlToImage, this.publishedAt, this.content});

  @override
  List<Object?> get props => [title, description, url, urlToImage, publishedAt, content];
}

class Source {
  final String? id;
  final String? name;

  const Source({this.id, this.name});

  factory Source.fromJson(Map<String, dynamic> map){
    return Source(
      id: map['id'],
      name: map['name']
    );
  }

  Map<String, dynamic> toJson(){
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
