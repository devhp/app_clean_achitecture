import 'dart:io';

import 'package:app_clean_architecture/core/constants/constants.dart';
import 'package:app_clean_architecture/core/resources/data_state.dart';
import 'package:app_clean_architecture/data/data_source/remote/news_api_service.dart';
import 'package:app_clean_architecture/data/models/article_model.dart';
import 'package:dio/dio.dart';

import '../../domain/repository/article_repository.dart';

class ArticleRepositoryImp implements ArticleRepository {
  final NewsApiService _newsApiService;

  ArticleRepositoryImp(this._newsApiService);

  @override
  Future<DataState<List<ArticleModel>>> getNewsArticles() async {
    try {
      final res = await _newsApiService.getNewsArticle(country, apiKey);
      if (res.response.statusCode == HttpStatus.ok) {
        return DataSuccess(res.data);
      } else {
        return DataFailed(DioException(
            requestOptions: res.response.requestOptions,
            error: res.response.statusMessage,
            response: res.response,
            type: DioExceptionType.badResponse));
      }
    } on DioException catch (e) {
      return DataFailed(e);
    }
  }
}
