import 'package:app_clean_architecture/core/constants/constants.dart';
import 'package:app_clean_architecture/data/models/article_model.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'news_api_service.g.dart';

@RestApi(baseUrl: baseUrl)
abstract class NewsApiService {
  factory NewsApiService(Dio dio) = _NewsApiService;

  @GET('/top-headlines')
  Future<HttpResponse<List<ArticleModel>>> getNewsArticle(
    @Query('country') String country,
    @Query('apiKey') String apiKey,
  );
}
