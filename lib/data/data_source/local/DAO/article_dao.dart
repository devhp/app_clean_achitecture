import 'package:app_clean_architecture/data/models/article_model.dart';
import 'package:floor/floor.dart';

@dao
abstract class ArticleDao{
  @insert
  Future<void> insertArticle(ArticleModel articleModel);

  @delete
  Future<void> deleteArticle(ArticleModel articleModel);
  
  @Query('SELECT * FROM article')
  Future<List<ArticleModel>> getArticles();
}